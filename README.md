# dnsmasq-with-cloudflare-doh-and-chinalist for ARM
A dockerfile for building a dnsmasq container with DoH and China Domain List.

# Usage

    mkdir docker-chinalist-dns && cd docker-chinalist-dns
    git clone https://xxxx.git .

    # docker buildx build -t dnsmasq-cnlist-arm --platform=linux/arm -o type=docker .
    docker buildx build -t dnsmasq-cnlist-arm64 --platform=linux/arm64 -o type=docker .
    docker run -d -p 53:53/udp dnsmasq-cnlist-arm64:latest

## Dockerhub Image

https://hub.docker.com/repository/docker/hvanke/dnsmasq-cnlist-arm
