FROM arm32v7/debian:jessie-slim

ENV CFD_URL https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-arm.tgz
ENV CFD_FILE cloudflared.tar.gz
ENV CDL_URL https://github.com/felixonmars/dnsmasq-china-list/archive/master.tar.gz
ENV CDL_FILE chinalist.tar.gz

ENV CHINA_DNS_SERVER 223.6.6.6

RUN apt-get update \
	&& apt-get install -y curl dnsmasq \
	&& apt-get autoremove -y \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app
# CloudFlare DNS over HTTPS
RUN curl -SL ${CFD_URL} -o ${CFD_FILE} && tar zxvf ${CFD_FILE} && rm -rf ${CFD_FILE} && cp ./cloudflared /usr/local/bin/ && chmod +x /usr/local/bin/cloudflared
# get China Domain List
RUN mkdir chinalist && cd chinalist \
	&& curl -SL ${CDL_URL} -o ${CDL_FILE} && tar zxvf ${CDL_FILE} --strip 1 && rm -rf ${CDL_FILE} \
	&& sed -e "s/114.114.114.114/$CHINA_DNS_SERVER/g" accelerated-domains.china.conf > accelerated-domains.china.dnsmasq.conf \
	&& cp -v accelerated-domains.china.dnsmasq.conf /etc/dnsmasq.d/ \
	&& cd /

ADD ./start.sh /app/start.sh

EXPOSE 53/tcp 53/udp
CMD /bin/sh start.sh
