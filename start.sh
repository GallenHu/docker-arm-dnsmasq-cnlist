#!/bin/sh

nohup /usr/local/bin/cloudflared proxy-dns --port 5300 --address 127.0.0.1 &

dnsmasq --conf-dir=/etc/dnsmasq.d/ --cache-size=25000 --keep-in-foreground --server="127.0.0.1#5300" --log-facility=/dev/stdout --no-resolv --user=root